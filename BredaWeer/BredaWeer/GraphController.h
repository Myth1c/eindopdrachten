//
//  GraphController.h
//  BredaWeer
//
//  Created by Remy Baratte on 10/25/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CorePlot-CocoaTouch.h>
#import "DataService.h"

@interface GraphController : UIViewController <CPTPlotDataSource>

#define LEFT_PADDING    55.0f
#define BOTTOM_PADDING  25.0f
#define LINE_WIDTH      2.5
#define LINE_COLOR      [CPTColor whiteColor]
#define CPTDecimalZero  CPTDecimalFromDouble(0.0)

@property (nonatomic, strong) CPTGraphHostingView *hostView;
@property (nonatomic, strong) DataService *dataService;

@end
