//
//  DataPoint.m
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import "DataPoint.h"
#import "TimeParser.h"

@implementation DataPoint

-(instancetype)initDataPointWithContentsOfDictionary:(NSDictionary*)dictionary
{
    if(self = [super init])
    {
        _value = [NSNumber numberWithDouble: [[dictionary objectForKey:@"value"] doubleValue]];
        _date = [TimeParser dateWithISOString:[dictionary objectForKey:@"at"]];
        return self;
    }
    return nil;
}

@end
