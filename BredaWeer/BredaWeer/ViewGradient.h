//
//  ViewGradient.h
//  BredaWeer
//
//  Created by Remy Baratte on 10/25/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewGradient : NSObject

+(CAGradientLayer *)cloudyGradient;
+(CAGradientLayer *)sunnyGradient;

@end
