//
//  DataService.m
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import "DataService.h"
#import "DataStream.h"
#import "DataRequest.h"

@implementation DataService

-(instancetype)init
{
    if(self = [super init])
    {
        _dataStreams = [[NSArray alloc] initWithObjects:[DataStream alloc], [DataStream alloc], [DataStream alloc], [DataStream alloc], [DataStream alloc], [DataStream alloc], nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataChanged:) name:@"DataChanged" object:nil];
        _dataRefresh = [NSTimer scheduledTimerWithTimeInterval:REFRESH_INTERVAL target:self selector:@selector(getCurrentAWSData) userInfo:nil repeats:YES];
        [_dataRefresh fire];
        return self;
    }
    return nil;
}

-(DataStream *)getAirpressureStream
{
    return [_dataStreams objectAtIndex:ARRAY_POS_AIR_PRESSURE];
}

-(DataStream *)getHumidityStream
{
    return [_dataStreams objectAtIndex:ARRAY_POS_HUMIDITY];
}

-(DataStream *)getRainrateStream
{
    return [_dataStreams objectAtIndex:ARRAY_POS_RAIN_RATE];
}

-(DataStream *)getTemperatureStream
{
    return [_dataStreams objectAtIndex:ARRAY_POS_TEMPERATURE];
}

-(DataStream *)getWinddirectionStream
{
    return [_dataStreams objectAtIndex:ARRAY_POS_WIND_DIR];
}

-(DataStream *)getWindspeedStream
{
    return [_dataStreams objectAtIndex:ARRAY_POS_WIND_SPEED];
}

-(void)getLastWeekData
{
    [self getAWSDataWithDuration:DURATION_WEEK];
}

-(void)getStreamDataWithValue:(NSString *)dataValue AndDuration:(NSString *)duration
{
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@%@%@", BASE_URL, DATASTREAMS, dataValue, JSON, DURATION, duration];
    DataRequest *request = [DataRequest alloc];
    [request requestDataWithUrl:url];
}

-(void)getCurrentStreamDataWithValue:(NSString *)dataValue
{
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@", BASE_URL, DATASTREAMS, dataValue, JSON];
    DataRequest *request = [[DataRequest alloc] init];
    [request requestDataWithUrl:url];
}

-(void)getCurrentAWSData
{
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, JSON];
    DataRequest *request = [[DataRequest alloc] init];
    [request requestDataWithUrl:url];
}

-(void)getAWSDataWithDuration:(NSString *)duration
{
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@", BASE_URL, JSON, DURATION, duration];
    DataRequest *request = [[DataRequest alloc] init];
    [request requestDataWithUrl:url];
}

-(void)dataChanged:(NSNotification *)notification
{
    if([[notification name] isEqualToString:@"DataChanged"])
    {
        _dataStreams = [[notification userInfo] objectForKey:@"Data"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DataReady" object:nil];
    }
}

@end
