//
//  GraphController.m
//  BredaWeer
//
//  Created by Remy Baratte on 10/25/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "GraphController.h"
#import "DataPoint.h"

@interface GraphController ()

@end

@implementation GraphController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataChanged:) name:@"DataReady" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_dataService getLastWeekData];
}

-(void)viewWillLayoutSubviews
{
    self.hostView.frame = self.view.bounds;
}

#pragma mark -
#pragma mark CorePlot Delegate Methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    if([plot.identifier isEqual:DATAVALUE_TEMPERATURE])
    {
        return [[[_dataService getTemperatureStream] dataPoints] count];
    }
    else if([plot.identifier isEqual:DATAVALUE_AIR_PRESSURE])
    {
        return [[[_dataService getAirpressureStream] dataPoints] count];
    }
    else if([plot.identifier isEqual:DATAVALUE_HUMIDITY])
    {
        return [[[_dataService getHumidityStream] dataPoints] count];
    }
    else if([plot.identifier isEqual:DATAVALUE_RAIN_RATE])
    {
        return [[[_dataService getRainrateStream] dataPoints] count];
    }
    else if([plot.identifier isEqual:DATAVALUE_WIND_SPEED])
    {
        return [[[_dataService getWindspeedStream] dataPoints] count];
    }
    else if([plot.identifier isEqual:DATAVALUE_WIND_DIR])
    {
        return [[[_dataService getWinddirectionStream] dataPoints] count];
    }
    return 0;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    switch(fieldEnum)
    {
        case CPTScatterPlotFieldX:
            return [NSNumber numberWithInt:index];
            break;
        case CPTScatterPlotFieldY:
            if([plot.identifier isEqual:DATAVALUE_TEMPERATURE])
            {
                return ((DataPoint *) [[[_dataService getTemperatureStream] dataPoints] objectAtIndex:index]).value;
            }
            else if([plot.identifier isEqual:DATAVALUE_AIR_PRESSURE])
            {
                return ((DataPoint *) [[[_dataService getAirpressureStream] dataPoints] objectAtIndex:index]).value;
            }
            else if([plot.identifier isEqual:DATAVALUE_HUMIDITY])
            {
                return ((DataPoint *) [[[_dataService getHumidityStream] dataPoints] objectAtIndex:index]).value;
            }
            else if([plot.identifier isEqual:DATAVALUE_RAIN_RATE])
            {
                return ((DataPoint *) [[[_dataService getRainrateStream] dataPoints] objectAtIndex:index]).value;
            }
            else if([plot.identifier isEqual:DATAVALUE_WIND_SPEED])
            {
                return ((DataPoint *) [[[_dataService getWindspeedStream] dataPoints] objectAtIndex:index]).value;
            }
            else if([plot.identifier isEqual:DATAVALUE_WIND_DIR])
            {
                return ((DataPoint *) [[[_dataService getWinddirectionStream] dataPoints] objectAtIndex:index]).value;
            }
            break;
        default:
            return [NSDecimalNumber zero];
    }
    return [NSDecimalNumber zero];
}

-(void)dataChanged:(NSNotification *)notification
{
    if([[notification name] isEqualToString:@"DataReady"])
    {
        [self initPlots];
    }
}

#pragma mark - Chart behavior
-(void)initPlots
{
    /* Initialize host view for the scatter plot. */
    self.hostView = [[CPTGraphHostingView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.hostView ];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

-(void)configureGraph
{
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    [graph applyTheme:[CPTTheme themeNamed:kCPTSlateTheme]];
    self.hostView.hostedGraph = graph;
    [graph.plotAreaFrame setPaddingLeft:LEFT_PADDING];
    [graph.plotAreaFrame setPaddingBottom:BOTTOM_PADDING];
    graph.defaultPlotSpace.allowsUserInteraction = YES;
}

-(void)configurePlots
{
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    for(DataStream *stream in _dataService.dataStreams)
    {
        CPTScatterPlot *feedPlot = [[CPTScatterPlot alloc] init];
        feedPlot.dataSource = self;
        feedPlot.identifier = stream.m_id;
        CPTMutableLineStyle *lineStyle = [feedPlot.dataLineStyle mutableCopy];
        lineStyle.lineWidth = LINE_WIDTH;
        lineStyle.lineColor = LINE_COLOR;
        [graph addPlot:feedPlot toPlotSpace:plotSpace];
    }
    [plotSpace scaleToFitPlots:[graph allPlots]];
}

-(void)configureAxes
{
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    CPTAxis *xAxis = axisSet.xAxis;
    CPTAxis *yAxis = axisSet.yAxis;
    xAxis.tickDirection = CPTSignNone;
    yAxis.tickDirection = CPTSignNone;
    /* Scale plot range dynamically based on retrieved data. */
    int xLength = [[[[_dataService dataStreams] firstObject] dataPoints] count];
    int yLength = [[_dataService getAirpressureStream].maxVal intValue];
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) self.hostView.hostedGraph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalZero length:CPTDecimalFromInt(xLength)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalZero length:CPTDecimalFromInt(yLength)];
    xAxis.labelingPolicy = yAxis.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
