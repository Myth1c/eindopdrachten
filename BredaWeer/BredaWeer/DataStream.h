//
//  DataStream.h
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataStream : NSObject

@property (nonatomic, strong) NSString *m_id;
@property (nonatomic, strong) NSString *version;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDictionary *unit;

@property (nonatomic, strong) NSNumber *currentValue;
@property (nonatomic, strong) NSNumber *minVal;
@property (nonatomic, strong) NSNumber *maxVal;

@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) NSMutableArray *dataPoints;

-(instancetype)initStreamWithContentsOfDictionary:(NSDictionary*)dictionary;
-(NSString *)getSymbol;

@end
