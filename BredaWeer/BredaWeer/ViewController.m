//
//  ViewController.m
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import "ViewController.h"
#import "GraphController.h"
#import "ViewGradient.h"
#import "DataStream.h"

@implementation ViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _dataService = [[DataService alloc] init];
    _weatherData = [NSMutableArray array];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataChanged:) name:@"DataReady" object:nil];
}

#pragma mark -
#pragma mark Gradient
-(void)viewWillAppear:(BOOL)animated
{
    CAGradientLayer *backgroundLayer = [ViewGradient sunnyGradient];
    backgroundLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer:backgroundLayer atIndex:0];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [[[self.view.layer sublayers] objectAtIndex:0] setFrame:self.view.bounds];
}

-(void)dataChanged:(NSNotification *)notification
{
    if([[notification name] isEqualToString:@"DataReady"])
    {
        [self updateWeatherData];
    }
}
#pragma mark -

-(void)updateWeatherData
{
    DataStream *dataStream = [_dataService getTemperatureStream];
    _temperatureLabel.text = [NSString stringWithFormat:@"%@ %@", dataStream.currentValue, [dataStream getSymbol]];
    dataStream = [_dataService getAirpressureStream];
    _airPressureLabel.text = [NSString stringWithFormat:@"%@%@", dataStream.currentValue, [dataStream getSymbol]];
    dataStream = [_dataService getHumidityStream];
    _humidityLabel.text = [NSString stringWithFormat:@"%@ %@", dataStream.currentValue, [dataStream getSymbol]];
    dataStream = [_dataService getWindspeedStream];
    _windspeedLabel.text = [NSString stringWithFormat:@"%@ %@", dataStream.currentValue, [dataStream getSymbol]];
    dataStream = [_dataService getRainrateStream];
    _rainrateLabel.text = [NSString stringWithFormat:@"%@ %@", dataStream.currentValue, [dataStream getSymbol]];
    dataStream = [_dataService getWinddirectionStream];
    _winddirLabel.text = [NSString stringWithFormat:@"%@%@", dataStream.currentValue, [dataStream getSymbol]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    GraphController *destination = [segue destinationViewController];
    destination.dataService = _dataService;
}

- (IBAction)TransitionToGraphs:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"GraphSegue" sender:sender];
}

@end
