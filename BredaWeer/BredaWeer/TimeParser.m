//
//  TimeParser.m
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import "TimeParser.h"

@implementation TimeParser

+(NSDate*)dateWithISOString:(NSString*)toParse
{
    NSDateFormatter *isoFormat = [[NSDateFormatter alloc] init];
    [isoFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    return [isoFormat dateFromString:toParse];
}

@end
