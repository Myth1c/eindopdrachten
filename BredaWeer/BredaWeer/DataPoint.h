//
//  DataPoint.h
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataPoint : NSObject

@property (nonatomic, strong) NSNumber *value;
@property (nonatomic, strong) NSDate *date;

-(instancetype)initDataPointWithContentsOfDictionary:(NSDictionary*)dictionary;


@end
