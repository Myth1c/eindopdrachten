//
//  ViewController.h
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataService.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) DataService *dataService;
@property (nonatomic, strong) NSMutableArray *weatherData;

@property (strong, nonatomic) IBOutlet UIImageView *weatherType;
@property (strong, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (strong, nonatomic) IBOutlet UILabel *airPressureLabel;
@property (strong, nonatomic) IBOutlet UILabel *humidityLabel;
@property (strong, nonatomic) IBOutlet UILabel *windspeedLabel;
@property (strong, nonatomic) IBOutlet UILabel *rainrateLabel;
@property (strong, nonatomic) IBOutlet UILabel *winddirLabel;

- (IBAction)TransitionToGraphs:(UIButton *)sender;

@end
