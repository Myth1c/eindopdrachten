//
//  DataRequest.m
//  BredaWeer
//
//  Created by Remy Baratte on 10/16/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "DataRequest.h"
#import "DataService.h"
#import "DataPoint.h"
#import "DataStream.h"
#import <AFHTTPRequestOperationManager.h>

@implementation DataRequest

-(instancetype)init
{
    if(self = [super init])
    {
        _result = [NSMutableArray array];
        return self;
    }
    return nil;
}

-(void)requestDataWithUrl:(NSString *)url
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:API_KEY forHTTPHeaderField:@"X-ApiKey"];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self parseJSONWithDictionary:(NSDictionary *)responseObject];
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void)parseJSONWithDictionary:(NSDictionary *)dict
{
    for(NSDictionary *dataStream in [dict objectForKey:@"datastreams"])
    {
        //NSLog(@"DataStream: %@", dataStream);
        [_result addObject:[[DataStream alloc] initStreamWithContentsOfDictionary:dataStream]];
    }
    NSDictionary *payload = [NSDictionary dictionaryWithObject:_result forKey:@"Data"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DataChanged" object:nil userInfo:payload];
}

@end
