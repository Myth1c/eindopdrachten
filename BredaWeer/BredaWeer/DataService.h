//
//  DataService.h
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataStream.h"

/* The strings for use in request URL's. */
#define BASE_URL    @"https://api.xively.com/v2/feeds/40053"
#define JSON        @".json"
#define DURATION    @"?duration="
#define DATASTREAMS @"/datastreams/"
/* Extra strings for specific durations. */
#define DURATION_WEEK   @"1week"
/* The strings for each of the datavalues. */
#define DATAVALUE_AIR_PRESSURE  @"airpressure"
#define DATAVALUE_HUMIDITY      @"hum"
#define DATAVALUE_RAIN_RATE     @"rainrate"
#define DATAVALUE_TEMPERATURE   @"temp"
#define DATAVALUE_WIND_DIR      @"winddir"
#define DATAVALUE_WIND_SPEED    @"windspeed"
/* The positions of the specific values inside the datastream. */
#define ARRAY_POS_AIR_PRESSURE  0
#define ARRAY_POS_HUMIDITY      1
#define ARRAY_POS_RAIN_RATE     2
#define ARRAY_POS_TEMPERATURE   3
#define ARRAY_POS_WIND_DIR      4
#define ARRAY_POS_WIND_SPEED    5
/* The amount of seconds for resfreshing data. */
#define REFRESH_INTERVAL        60.0

@interface DataService : NSObject

@property (nonatomic, strong) NSArray *dataStreams;
@property (nonatomic, strong) NSTimer *dataRefresh;

-(void)getLastWeekData;
-(DataStream *)getAirpressureStream;
-(DataStream *)getHumidityStream;
-(DataStream *)getRainrateStream;
-(DataStream *)getTemperatureStream;
-(DataStream *)getWinddirectionStream;
-(DataStream *)getWindspeedStream;

@end
