//
//  TimeParser.h
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeParser : NSObject

+(NSDate*)dateWithISOString:(NSString*)toParse;

@end
