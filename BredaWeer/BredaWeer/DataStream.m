//
//  DataStream.m
//  BredaWeer
//
//  Created by atmstudent on 10/8/13.
//  Copyright (c) 2013 Max Maes. All rights reserved.
//

#import "DataStream.h"
#import "TimeParser.h"
#import "DataPoint.h"

@implementation DataStream

-(instancetype)initStreamWithContentsOfDictionary:(NSDictionary*)dictionary
{
    if(self = [super init])
    {
        _dataPoints = [NSMutableArray array];
        _currentValue = [NSNumber numberWithDouble:[[dictionary objectForKey:@"current_value"] doubleValue]];
        _minVal = [NSNumber numberWithDouble:[[dictionary objectForKey:@"min_value"] doubleValue]];
        _maxVal = [NSNumber numberWithDouble:[[dictionary objectForKey:@"max_value"] doubleValue]];
        
        _m_id = [dictionary objectForKey:@"id"];
        _tags = [dictionary objectForKey:@"tags"];
        _unit = [dictionary objectForKey:@"unit"];
        _date = [TimeParser dateWithISOString:[dictionary objectForKey:@"at"]];
        for(NSDictionary *datapoint in [dictionary objectForKey:@"datapoints"])
        {
            [_dataPoints addObject:[[DataPoint alloc] initDataPointWithContentsOfDictionary:datapoint]];
        }
        return self;
    }
    return nil;
}

-(NSString *)getSymbol
{
    return [_unit objectForKey:@"symbol"];
}

@end
