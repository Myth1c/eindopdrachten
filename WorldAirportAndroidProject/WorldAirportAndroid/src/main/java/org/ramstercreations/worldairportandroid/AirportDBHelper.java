package org.ramstercreations.worldairportandroid;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Remy Baratte on 26-10-13.
 */
public class AirportDBHelper extends SQLiteOpenHelper
{
    public static int COLUMN_ID = 0;
    public static int COLUMN_NAME = 1;
    public static int COLUMN_LAT = 2;
    public static int COLUMN_LONG = 3;
    public static int COLUMN_ELE = 4;
    public static int COLUMN_COUNTRY = 5;
    public static int COLUMN_MUN = 6;

    private static AirportDBHelper dbInstance;
    private static final int DB_VERSION = 1;
    private Context helperContext;

    public static AirportDBHelper getInstance(Context context)
    {
        if(dbInstance == null)
        {
            dbInstance = new AirportDBHelper(context);
        }
        return dbInstance;
    }

    private AirportDBHelper(Context context)
    {
        super(context, context.getString(R.string.db_name), null, DB_VERSION);
        helperContext = context;
        if(!dbExists())
        {
            Log.i(helperContext.getString(R.string.db_helper_tag), "No Database found, creating...");
            try
            {
                copyDatabase();
            }
            catch(IOException ioe)
            {
                Log.e(helperContext.getString(R.string.db_helper_tag), ioe.getMessage());
            }
        }
    }

    @Override public void onCreate(SQLiteDatabase db)
    {

    }

    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        if(newVersion > oldVersion)
            onCreate(db);
    }

    private boolean dbExists()
    {
        SQLiteDatabase checkDB = null;
        try
        {
            checkDB = SQLiteDatabase.openDatabase(helperContext.getString(R.string.db_file), null, SQLiteDatabase.OPEN_READONLY);
        }
        catch (SQLiteException sqle)
        {
            //Database doesn't exist.
        }
        if (checkDB != null)
        {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDatabase() throws IOException
    {
        File outputFile = new File(helperContext.getString(R.string.db_file));
        outputFile.getParentFile().mkdirs();
        outputFile.createNewFile();

        int ResourceList[] = new int[] { R.raw.airports1, R.raw.airports2, R.raw.airports3, R.raw.airports4 };
        OutputStream databaseOutputStream = new FileOutputStream(helperContext.getString(R.string.db_file));
        for (int FileId : ResourceList)
        {
            InputStream inputFile = helperContext.getResources().openRawResource(FileId);
            int TotalLength = inputFile.available();
            byte[] buffer = new byte[TotalLength];
            inputFile.read(buffer);
            databaseOutputStream.write(buffer);
            inputFile.close();
        }
        databaseOutputStream.close();
    }

    public Cursor getWordMatches(String query, String[] columns)
    {
        String selection = helperContext.getString(R.string.airport_name) + " LIKE ?";
        String [] selectionArgs = new String[] { query + "%" };
        return query(selection, selectionArgs, columns);
    }

    private Cursor query(String selection, String[] selectionArgs, String[] columns)
    {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(helperContext.getString(R.string.db_table_name));
        Cursor cursor = queryBuilder.query(getReadableDatabase(), columns, selection, selectionArgs, null, null,  helperContext.getString(
                R.string.airport_name) + " ASC");
        if(cursor == null)
            return null;
        else if(!cursor.moveToFirst())
        {
            cursor.close();
            return null;
        }
        return cursor;
    }

    public Cursor getAirportList()
    {
        return query(null, null, new String[] { helperContext.getString(R.string.airport_name), helperContext.getString(R.string.airport_id) });
    }

    public Cursor getAirport(String _id)
    {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(helperContext.getString(R.string.db_table_name));
        Cursor cursor = queryBuilder.query(getWritableDatabase(), null, helperContext.getString(R.string.airport_id) + " = '" + _id + "'", null, null, null, null);
        if(cursor == null)
            return null;
        else if(!cursor.moveToFirst())
        {
            cursor.close();
            return null;
        }
        return cursor;
    }
}
