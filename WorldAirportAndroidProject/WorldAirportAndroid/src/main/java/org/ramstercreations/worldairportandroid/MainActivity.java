package org.ramstercreations.worldairportandroid;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends Activity implements SearchView.OnQueryTextListener
{
    private SimpleCursorAdapter listAdapter;
    private AirportDBHelper dbHelper;
    private SearchView airportSearch;
    private ListView airportList;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        airportSearch = (SearchView)findViewById(R.id.airportSearch);
        context = getApplicationContext();
        dbHelper = AirportDBHelper.getInstance(context);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        listAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, dbHelper.getAirportList(), new String[] { context.getString(
                R.string.airport_name) }, new int[] { android.R.id.text1 }, 0);
        airportList = (ListView)findViewById(R.id.airportList);
        airportList.setAdapter(listAdapter);
        airportList.setTextFilterEnabled(false);
        airportList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent mapIntent = new Intent(context, WorldMapActivity.class);
                /* Retrieve the name from the cursor and pull the other information from the database. */
                Cursor airport = (Cursor) airportList.getAdapter().getItem(position);
                mapIntent.putExtra(context.getString(R.string.airport_extra), airport.getString(airport.getColumnIndex(context.getString(R.string.airport_id))));
                startActivity(mapIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager airportSearchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        airportSearch = (SearchView) menu.findItem(R.id.airportSearch).getActionView();
        airportSearch.setSearchableInfo(airportSearchManager.getSearchableInfo(getComponentName()));
        airportSearch.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override public boolean onQueryTextSubmit(String query)
    {
        return false;
    }

    @Override public boolean onQueryTextChange(final String newText)
    {
        /* Update the search filter. */
        AsyncTask searchTask = new AsyncTask<Object, Void, Cursor>()
        {
            @Override protected void onPostExecute(Cursor cursor)
            {
                SimpleCursorAdapter listAdapter = (SimpleCursorAdapter) airportList.getAdapter();
                listAdapter.changeCursor(cursor);
            }

            @Override protected Cursor doInBackground(Object[] params)
            {
                return dbHelper.getWordMatches(newText, null);
            }
        };
        searchTask.execute();
        return true;
    }
}
