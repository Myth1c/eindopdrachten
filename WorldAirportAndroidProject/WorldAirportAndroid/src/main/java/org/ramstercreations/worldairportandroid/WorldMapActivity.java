package org.ramstercreations.worldairportandroid;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

public class WorldMapActivity extends Activity
{
    private AirportDBHelper dbHelper;
    private GoogleMap worldMap;
    private Context context;
    private String airport_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_map);
        context = getApplicationContext();
        dbHelper = AirportDBHelper.getInstance(context);
        airport_id = getIntent().getStringExtra(context.getString(R.string.airport_extra));
    }

    @Override
    public void onResume()
    {
        super.onResume();
        initializeMap();
        airportInfo();
    }

    private void initializeMap()
    {
        if(worldMap == null)
        {
            worldMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.worldMap)).getMap();
            if (worldMap == null)
                Log.d(context.getString(R.string.world_map_tag), "World map is null!");
            worldMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }

    private void airportInfo()
    {
        Cursor from = dbHelper.getAirport(airport_id);
        Cursor to = dbHelper.getAirport(context.getString(R.string.schiphol_airport));
        if(from != null && to != null)
        {
            double fromLat = from.getDouble(from.getColumnIndex(context.getString(R.string.airport_lat)));
            double fromLong = from.getDouble(from.getColumnIndex(context.getString(R.string.airport_long)));
            double toLat = to.getDouble(to.getColumnIndex(context.getString(R.string.airport_lat)));
            double toLong = to.getDouble(to.getColumnIndex(context.getString(R.string.airport_long)));
            worldMap.addPolyline(new PolylineOptions().add(new LatLng(fromLat, fromLong), new LatLng(toLat, toLong)).geodesic(true).color(Color.RED).width(5));
            from.close();
            to.close();
        }
        else
        {
            Log.w(context.getString(R.string.world_map_tag), "Cursor from or cursor to is null, couldn't complete request.");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.world_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
