package nl.myth1cproductions.bredaweerandroid;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import nl.myth1cproductions.bredaweerandroid.data.DataService;
import nl.myth1cproductions.bredaweerandroid.data.OnDataChangedListener;
import nl.myth1cproductions.bredaweerandroid.data.model.DataStream;

public class MainActivity extends Activity implements OnDataChangedListener {
    private DataService dataService;
    private ProgressDialog refreshing;
    private int currentGradient;
    private int currentImage;
    private int currentActionBar;

    private static int UPDATE_START = 0;
    private static int UPDATE_INTERVAL = 60000;

    private DataUpdateTimerTask updateTimerTask;
    private Timer updateTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainscreen);

        if (savedInstanceState != null) {
            dataService = savedInstanceState.getParcelable("dataservice");
            dataService.addDataChangeListener(this); //make sure there we are set as listener, it will get ignored when the list of listeners already contains this object.
            setData();
        } else {
            dataService = new DataService(); //pass a reference to self so we can call onDataChanged from the DataService when it has updated a datastream
            dataService.addDataChangeListener(this);
            if (NetworkConnectionAvailable()) {
                dataService.getCurrentAWSData();
                refreshing = new ProgressDialog(this);
                refreshing.setMessage("Please wait...");
                refreshing.setCancelable(false);
                refreshing.show();
            } else {
                String text = getResources().getString(R.string.no_connection);
                Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        //setAppearance(R.drawable.gradient_sunny, R.drawable.zon, R.drawable.color_sunny);

        updateTimerTask = new DataUpdateTimerTask(dataService);
        updateTimer = new Timer();
        updateTimer.scheduleAtFixedRate(updateTimerTask, UPDATE_START, UPDATE_INTERVAL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        ColorDrawable colorDrawable = (ColorDrawable) getResources().getDrawable(
                R.drawable.color_sunny);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                //nope, not doing this
                return true;
            case R.id.action_graphs:
                Intent graphIntent = new Intent(this, GraphActivity.class);
                graphIntent.putExtra("gradient", currentGradient);
                graphIntent.putExtra("actionbar", currentActionBar);
                graphIntent.putExtra("dataservice", dataService);
                startActivity(graphIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putParcelable("dataservice", dataService);
    }

    public void setData() {
        TextView textView = (TextView) findViewById(R.id.temperature_value);
        DataStream dataStream = dataService.getTemperatureStream();
        String toSet = dataStream.getCurrentValue() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.avg_temperature_value);
        toSet = dataStream.getAvarage() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.windspeed_value);
        dataStream = dataService.getWindspeedStream();
        toSet = dataStream.getCurrentValue() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.avg_windspeed_value);
        toSet = dataStream.getAvarage() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.winddirection_value);
        dataStream = dataService.getWinddirectionStream();
        toSet = dataStream.getCurrentValue() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.avg_winddirection_value);
        toSet = dataStream.getAvarage() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        final ImageView winddir = (ImageView) findViewById(R.id.icon_winddirection);
        final float angle = (float)dataStream.getCurrentValue();
        final float pivX = winddir.getDrawable().getBounds().width()/2.0f;
        final float pivY = winddir.getDrawable().getBounds().height()/2.0f;
        RotateAnimation anim = new RotateAnimation(0f, angle, pivX, pivY );
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(0);
        anim.setDuration(1000);
        anim.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Matrix matrix=new Matrix();
                winddir.setScaleType(ImageView.ScaleType.MATRIX);   //required
                matrix.postRotate((float) angle, pivX, pivY);
                winddir.setImageMatrix(matrix);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        // Start animating the image
        winddir.startAnimation(anim);

        // Later.. stop the animation
       // splash.setAnimation(null);

       /* ImageView winddir = (ImageView) findViewById(R.id.icon_winddirection);
        Matrix matrix = new Matrix();
        winddir.setScaleType(ImageView.ScaleType.MATRIX);   //required
        matrix.postRotate((float) 90, winddir.getDrawable().getBounds().width()/2, winddir.getDrawable().getBounds().height()/2);
        winddir.setImageMatrix(matrix);*/

        textView = (TextView) findViewById(R.id.humidity_value);
        dataStream = dataService.getHumidityStream();
        toSet = dataStream.getCurrentValue() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.avg_humidity_value);
        toSet = dataStream.getAvarage() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.rainrate_value);
        dataStream = dataService.getRainrateStream();
        toSet = dataStream.getCurrentValue() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.avg_rainrate_value);
        toSet = dataStream.getAvarage() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.airpressure_value);
        dataStream = dataService.getAirpressureStream();
        toSet = dataStream.getCurrentValue() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        textView = (TextView) findViewById(R.id.avg_airpressure_value);
        toSet = dataStream.getAvarage() + " " + dataStream.getSymbol();
        textView.setText(toSet);

        updateView();
    }

    public void updateView()
    {
        double temperature = dataService.getTemperatureStream().getCurrentValue();
        double windspeed = dataService.getWindspeedStream().getCurrentValue();
        double rainrate = dataService.getRainrateStream().getCurrentValue();

        if(temperature > 10 && windspeed < 1 && rainrate == 0)
        {
            setAppearance(R.drawable.gradient_sunny, R.drawable.zon, R.drawable.color_sunny);
        }
        if(temperature <= 10 && windspeed >= 0 && rainrate < 2)
        {
            setAppearance(R.drawable.gradient_cloudy, R.drawable.wolk, R.drawable.color_cloudy);
        }
        if(rainrate >= 2)
        {
            setAppearance(R.drawable.gradient_rain, R.drawable.rain_heavy, R.drawable.color_rain);
        }
        if(windspeed > 1 && rainrate >= 5)
        {
            setAppearance(R.drawable.gradient_thunder, R.drawable.thunder, R.drawable.color_thunder);
        }
    }

    public void setDebugData() {
        TextView textView = (TextView) findViewById(R.id.debug_currentTemperatureValue);
        DataStream dataStream = dataService.getTemperatureStream();
        textView.setText("" + dataStream.getCurrentValue());

        textView = (TextView) findViewById(R.id.debug_currentWindspeedValue);
        dataStream = dataService.getWindspeedStream();
        textView.setText("" + dataStream.getCurrentValue());

        textView = (TextView) findViewById(R.id.debug_currentWindDirectionValue);
        dataStream = dataService.getWinddirectionStream();
        textView.setText("" + dataStream.getCurrentValue());

        textView = (TextView) findViewById(R.id.debug_currentHumidityValue);
        dataStream = dataService.getHumidityStream();
        textView.setText("" + dataStream.getCurrentValue());

        textView = (TextView) findViewById(R.id.debug_currentRainrateValue);
        dataStream = dataService.getRainrateStream();
        textView.setText("" + dataStream.getCurrentValue());

        textView = (TextView) findViewById(R.id.debug_currentAirpressureValue);
        dataStream = dataService.getAirpressureStream();
        textView.setText("" + dataStream.getCurrentValue());
    }

    @Override
    public void onDataChanged() {
        //Reload the data on the view;
        setData();
        if(refreshing != null)
        {
            refreshing.hide();
        }
    }

    public void changeGradient(View view) {
        int background = 0;
        int image = 0;
        int actionBarColor = R.drawable.color_sunny;
        switch (view.getId()) {
            case R.id.debug_gradientsunny:
                background = R.drawable.gradient_sunny;
                image = R.drawable.zon;
                actionBarColor = R.drawable.color_sunny;
                break;
            case R.id.debug_gradientcloud:
                background = R.drawable.gradient_cloudy;
                image = R.drawable.wolk;
                actionBarColor = R.drawable.color_cloudy;
                break;
            case R.id.debug_gradientrain:
                background = R.drawable.gradient_rain;
                image = R.drawable.rain_heavy;
                actionBarColor = R.drawable.color_rain;
                break;
            case R.id.debug_gradientthunder:
                background = R.drawable.gradient_thunder;
                image = R.drawable.thunder;
                actionBarColor = R.drawable.color_thunder;
                break;
            default:
                background = R.drawable.gradient_sunny;
                image = R.drawable.zon;
                actionBarColor = R.drawable.color_sunny;
                break;
        }
        setAppearance(background, image, actionBarColor);
    }

    private void setAppearance(int gradient, int image, int actionbarColor) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.mainscreen_root);
        ImageView weatherImage = (ImageView) findViewById(R.id.weathericon);

        Drawable fromGradient = layout.getBackground();
        Drawable toGradient = getResources().getDrawable(gradient);

        layout.setBackgroundResource(gradient);
        weatherImage.setImageResource(image);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            ColorDrawable colorDrawable = (ColorDrawable) getResources().getDrawable(actionbarColor);
            ActionBar actionBar;
            actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setBackgroundDrawable(colorDrawable);
                actionBar.setDisplayShowTitleEnabled(false); //Hackaround because the color won't update otherwise when called outside of onCreate.
                actionBar.setDisplayShowTitleEnabled(true);
            }
        }
        currentGradient = gradient;
        currentImage = image;
        currentActionBar = actionbarColor;
    }

    private TransitionDrawable createTransition(Drawable from, Drawable to) {
        TransitionDrawable drawable = new TransitionDrawable(new Drawable[]{from, to});
        drawable.setCrossFadeEnabled(true);
        return drawable;
    }

    private boolean NetworkConnectionAvailable() {
        boolean hasWifi = false;
        boolean hasMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                if (ni.isConnected()) {
                    hasWifi = true;
                }
            }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                if (ni.isConnected()) {
                    hasMobile = true;
                }
            }
        }
        return hasWifi || hasMobile;
    }

    private class DataUpdateTimerTask extends TimerTask {
        private DataService dataService;

        public DataUpdateTimerTask(DataService dataService) {
            this.dataService = dataService;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dataService.getCurrentAWSData();
                }
            });
        }
    }
}
