package nl.myth1cproductions.bredaweerandroid.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Max on 30-9-13.
 */
public class DataStream implements Parcelable {
    private String id;
    private double currentValue;
    private String date;
    private double minVal;
    private double maxVal;
    private String[] tags;
    private HashMap<String, String> unit;
    private String version;
    private ArrayList<DataPoint> dataPoints;
    private boolean upToDate;

    /**
     * Parses the given JSON object to a datastream.
     *
     * @param json
     */
    public DataStream(JSONObject json) {
        try {
            this.id = json.getString("id");
            this.currentValue = json.getDouble("current_value");
            this.date = json.getString("at");
            this.minVal = json.getDouble("min_value");
            this.maxVal = json.getDouble("max_value");
            JSONArray tagsArray = json.getJSONArray("tags");
            this.tags = new String[tagsArray.length()];
            for (int i = 0; i < tagsArray.length(); i++) {
                this.tags[i] = (tagsArray.get(i).toString());
            }
            JSONObject units = json.getJSONObject("unit");
            Iterator<String> unitKeys = units.keys();
            this.unit = new HashMap<String, String>();
            String key = "";
            while (unitKeys.hasNext()) {
                key = unitKeys.next();
                this.unit.put(key, units.getString(key));
            }

            this.dataPoints = new ArrayList<DataPoint>();
            if (json.has("datapoints")) {
                JSONArray datapoints = json.getJSONArray("datapoints");
                for (int i = 0; i < datapoints.length(); i++) {
                    this.dataPoints.add(new DataPoint(datapoints.getJSONObject(i)));
                }
            }

            if (json.has("version")) {
                this.version = json.getString("version");
            }
        } catch (JSONException jse) {
            jse.printStackTrace();
        }
        upToDate = true;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setCurrentValue(float currentValue) {
        this.currentValue = currentValue;
    }

    public double getCurrentValue() {
        return this.currentValue;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return this.date;
    }

    public void setMinVal(float minVal) {
        this.minVal = minVal;
    }

    public double getMinVal() {
        return this.minVal;
    }

    public void setMaxVal(float maxVal) {
        this.maxVal = maxVal;
    }

    public double getMaxVal() {
        return this.maxVal;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String[] getTags() {
        return this.tags;
    }

    public void setUnit(HashMap<String, String> unit) {
        this.unit = unit;
    }

    private HashMap<String, String> getUnit() {
        return this.unit;
    }

    public String getSymbol() {
        return getUnit().get("symbol");
    }

    public String getSymbolLabel() {
        return getUnit().get("label");
    }

    public ArrayList<DataPoint> getDataPoints() {
        return this.dataPoints;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return this.version;
    }

    /**
     * Calculates the average of the values in the values in this datastream.
     * @return
     */
    public double getAvarage()
    {
        double total = 0;
        for(DataPoint point : dataPoints)
        {
            total += point.getValue();
        }

        return Math.round(total / dataPoints.size());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeDouble(currentValue);
        out.writeString(date);
        out.writeDouble(minVal);
        out.writeDouble(maxVal);
        out.writeStringArray(tags);
        out.writeSerializable(unit);
        out.writeString(version);
        out.writeList(dataPoints);
        out.writeInt(upToDate ? 1 : 0);
    }

    public static final Parcelable.Creator<DataStream> CREATOR = new Parcelable.Creator<DataStream>() {
        public DataStream createFromParcel(Parcel in) {
            return new DataStream(in);
        }

        public DataStream[] newArray(int size) {
            return new DataStream[size];
        }
    };

    private DataStream(Parcel in) {
        id = in.readString();
        currentValue = in.readDouble();
        date = in.readString();
        minVal = in.readDouble();
        maxVal = in.readDouble();
        tags = in.createStringArray();
        unit = (HashMap<String, String>) in.readSerializable();
        version = in.readString();
        dataPoints = new ArrayList<DataPoint>();
        in.readList(dataPoints, DataPoint.class.getClassLoader());
        upToDate = in.readInt() == 1;
    }
}


