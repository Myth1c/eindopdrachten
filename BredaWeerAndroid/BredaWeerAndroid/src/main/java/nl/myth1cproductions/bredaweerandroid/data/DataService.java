package nl.myth1cproductions.bredaweerandroid.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;

import nl.myth1cproductions.bredaweerandroid.data.model.DataStream;

/**
 * Created by Max on 30-9-13.
 */
public class DataService implements OnDataRequestCompletedListener, Parcelable {
    public static final String DATAVALUE_AIRPRESSURE = "airpressure";
    public static final String DATAVALUE_HUMIDITY = "hum";
    public static final String DATAVALUE_RAINRATE = "rainrate";
    public static final String DATAVALUE_TEMPERATURE = "temp";
    public static final String DATAVALUE_WINDDIR = "winddir";
    public static final String DATAVALUE_WINDSPEED = "windspeed";

    private static final int ARRAYPOSITION_AIRPRESSURE = 0;
    private static final int ARRAYPOSITION_HUMIDITY = 1;
    private static final int ARRAYPOSITION_RAINRATE = 2;
    private static final int ARRAYPOSITION_TEMPERATURE = 3;
    private static final int ARRAYPOSITION_WINDDIR = 4;
    private static final int ARRAYPOSITION_WINDSPEED = 5;

    private final String BASEURL = "https://api.xively.com/v2/feeds/40053";
    private final String JSON = ".json";
    private final String DURATION = "?duration=";
    private final String DATASTREAMS = "/datastreams/";

    private DataStream[] dataStreams;
    private ArrayList<OnDataChangedListener> dataChangedListeners;

    private static DataService m_instance;

    public DataService() {
        dataStreams = new DataStream[6];
        dataChangedListeners = new ArrayList<OnDataChangedListener>();
    }

    public void addDataChangeListener(OnDataChangedListener listener) {
        if(dataChangedListeners == null)
        {
            dataChangedListeners = new ArrayList<OnDataChangedListener>();
        }
        if (!dataChangedListeners.contains(listener)) {
            dataChangedListeners.add(listener);
        }
    }

    public DataStream getAirpressureStream() {
        return this.dataStreams[ARRAYPOSITION_AIRPRESSURE];
    }

    public DataStream getHumidityStream() {
        return this.dataStreams[ARRAYPOSITION_HUMIDITY];
    }

    public DataStream getRainrateStream() {
        return this.dataStreams[ARRAYPOSITION_RAINRATE];
    }

    public DataStream getTemperatureStream() {
        return this.dataStreams[ARRAYPOSITION_TEMPERATURE];
    }

    public DataStream getWinddirectionStream() {
        return this.dataStreams[ARRAYPOSITION_WINDDIR];
    }

    public DataStream getWindspeedStream() {
        return this.dataStreams[ARRAYPOSITION_WINDSPEED];
    }

    public void getStreamData(String dataValue, String duration) {
        String url = BASEURL + DATASTREAMS + dataValue + JSON + DURATION + duration;
        DataRequest request = new DataRequest(this);
        request.execute(url);
    }

    public void getCurrentStreamData(String dataValue) {
        String url = BASEURL + DATASTREAMS + dataValue + JSON;
        DataRequest request = new DataRequest(this);
        request.execute(url);
    }

    public void getCurrentAWSData() {
        getAWSData("7days");
    }

    public void getAWSData(String duration) {
        String url = BASEURL + JSON + DURATION + duration;
        DataRequest request = new DataRequest(this);
        request.execute(url);
    }

    @Override
    public void onDataRequestCompleted(ArrayList<DataStream> result) {
        if (result != null) {
            for (DataStream stream : result) {
                if (stream.getId().equalsIgnoreCase(DATAVALUE_AIRPRESSURE)) {
                    dataStreams[ARRAYPOSITION_AIRPRESSURE] = stream;
                } else if (stream.getId().equalsIgnoreCase(DATAVALUE_HUMIDITY)) {
                    dataStreams[ARRAYPOSITION_HUMIDITY] = stream;
                } else if (stream.getId().equalsIgnoreCase(DATAVALUE_RAINRATE)) {
                    dataStreams[ARRAYPOSITION_RAINRATE] = stream;
                } else if (stream.getId().equalsIgnoreCase(DATAVALUE_TEMPERATURE)) {
                    dataStreams[ARRAYPOSITION_TEMPERATURE] = stream;
                } else if (stream.getId().equalsIgnoreCase(DATAVALUE_WINDDIR)) {
                    dataStreams[ARRAYPOSITION_WINDDIR] = stream;
                } else if (stream.getId().equalsIgnoreCase(DATAVALUE_WINDSPEED)) {
                    dataStreams[ARRAYPOSITION_WINDSPEED] = stream;
                }
            }
        }
        for (OnDataChangedListener listener : dataChangedListeners) {
            listener.onDataChanged();
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeArray(dataStreams);
    }

    public static final Parcelable.Creator<DataService> CREATOR = new Parcelable.Creator<DataService>() {
        public DataService createFromParcel(Parcel in) {
            return new DataService(in);
        }

        public DataService[] newArray(int size) {
            return new DataService[size];
        }
    };

    private DataService(Parcel in) {
        Object[] OBJarray = in.readArray(DataStream.class.getClassLoader());

        dataStreams = Arrays.copyOf(OBJarray, OBJarray.length, DataStream[].class);

    }
}