package nl.myth1cproductions.bredaweerandroid.charts;

/**
 * Created by Max on 21-10-13.
 */

import android.content.Context;
import android.graphics.Color;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.myth1cproductions.bredaweerandroid.data.model.DataPoint;
import nl.myth1cproductions.bredaweerandroid.data.model.DataStream;

/**
 * Airpressure chart.
 */
public class AirpressureChart extends AbstractChart {
    private List<double[]> values;
    private List<Date[]> dates;
    private double minVal = 1050;
    private double maxVal = 950;

    public AirpressureChart(DataStream stream) {
        values = new ArrayList<double[]>();
        dates = new ArrayList<Date[]>();
        double[] valuesArray = new double[stream.getDataPoints().size()];
        Date[] datesArray = new Date[stream.getDataPoints().size()];
        for (int i = 0; i < stream.getDataPoints().size(); i++) {
            DataPoint point = stream.getDataPoints().get(i);
            double value = point.getValue();
            valuesArray[i] = value;
            datesArray[i] = point.getDate();

            //We do not use the max and min val provided by xively since they are not reliable.
            if (value > maxVal) {
                maxVal = value;
            }
            if (value < minVal) {
                minVal = value;
            }
        }
        values.add(valuesArray);
        dates.add(datesArray);
    }

    /**
     * Returns the chart name.
     *
     * @return the chart name
     */
    public String getName() {
        return "Airpressure";
    }

    /**
     * Returns the chart name.
     *
     * @return the chart name
     */
    public String getXName() {
        return "Dates";
    }

    /**
     * Returns the chart name.
     *
     * @return the chart name
     */
    public String getYName() {
        return "hPa";
    }

    /**
     * Returns the chart description.
     *
     * @return the chart description
     */
    public String getDesc() {
        return "The airpressure chart for the given values.";
    }

    /**
     * Executes the chart demo.
     *
     * @param context the context
     * @return the built intent
     */
    public GraphicalView execute(Context context) {
        String[] titles = new String[]{getName()};

        int[] colors = new int[]{Color.RED};
        PointStyle[] styles = new PointStyle[]{PointStyle.POINT};
        XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles);

        setChartSettings(renderer,
                getName(),
                getXName(),
                getYName(),
                dates.get(0)[0].getTime(),
                dates.get(0)[dates.get(0).length - 1].getTime(),
                minVal, maxVal,
                Color.WHITE,
                Color.WHITE,
                Color.LTGRAY,
                Color.RED,
                Color.RED,
                Color.TRANSPARENT);
        renderer.setShowGrid(true);
        renderer.setGridColor(Color.LTGRAY);
        renderer.setYLabels(10);
        renderer.setXRoundedLabels(false);

        return ChartFactory.getTimeChartView(context, buildDateDataset(titles, dates, values), renderer, "HH:mm:ss dd MMM");
    }

}

