package nl.myth1cproductions.bredaweerandroid.data;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import nl.myth1cproductions.bredaweerandroid.data.model.DataStream;

/**
 * Created by Max on 4-10-13.
 */
public class DataRequest extends AsyncTask<String, Void, Void> {
    private final String APIKEY = "P_btPeh4FcS-eBfr4NheLke2QvSSAKxmZ0hRQ2taTGlxYz0g";
    private static final String TAG = "ASyncCallAWS";
    private static final int SOCKET_TIMEOUT = 20000;
    private static final int CONNECTION_TIMEOUT = 5000;

    private OnDataRequestCompletedListener onDataRequestCompletedListenerListener;
    private ArrayList<DataStream> result;

    public DataRequest(OnDataRequestCompletedListener onDataRequestCompletedListenerListener) {
        this.onDataRequestCompletedListenerListener = onDataRequestCompletedListenerListener;
    }

    @Override
    protected Void doInBackground(String... url) {
        String toRequest = url[0]; //We only support 1 request at a time.
        doHTTPRequest(toRequest);
        return null;
    }

    @Override
    protected void onPostExecute(Void unused) {
        onDataRequestCompletedListenerListener.onDataRequestCompleted(this.result);
    }

    private void doHTTPRequest(String url) {

        try {
            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            // The default value is zero, that means the timeout is not used.
            int timeoutConnection = CONNECTION_TIMEOUT;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = SOCKET_TIMEOUT;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            HttpClient client = new DefaultHttpClient(httpParameters);

            HttpGet get = new HttpGet(url);
            get.addHeader("X-ApiKey", APIKEY);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String httpResult = client.execute(get, responseHandler);
            Log.d(TAG, httpResult);
            parseJSON(httpResult);
        } catch (ClientProtocolException cpe) {
            cpe.printStackTrace();
        } catch (SocketTimeoutException ste) {
            ste.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void parseJSON(String jsonString) {
        JSONObject json;
        try {
            result = new ArrayList<DataStream>();
            json = new JSONObject(jsonString);
            if (json.has("datastreams")) {
                JSONArray jsonStreams = json.getJSONArray("datastreams");
                {
                    for (int i = 0; i < jsonStreams.length(); i++) {
                        JSONObject stream = jsonStreams.getJSONObject(i);
                        DataStream dataStream = new DataStream(stream);
                        result.add(dataStream);
                    }
                }
            } else {
                result.add(new DataStream(json));
            }
        } catch (JSONException jsonex) {
            jsonex.printStackTrace();
        }
    }
}
