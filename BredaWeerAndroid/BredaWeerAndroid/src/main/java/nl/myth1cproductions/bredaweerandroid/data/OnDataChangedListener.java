package nl.myth1cproductions.bredaweerandroid.data;

/**
 * Created by Max on 4-10-13.
 */
public interface OnDataChangedListener {
    void onDataChanged();
}
