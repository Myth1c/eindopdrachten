package nl.myth1cproductions.bredaweerandroid.charts;

import android.content.Context;

import org.achartengine.GraphicalView;

import nl.myth1cproductions.bredaweerandroid.data.model.DataStream;

/**
 * Created by Max on 21-10-13.
 */
public class ChartFactory {
    /**
     * Factory function that creates and returns the appropriate chart for the given datastream.
     *
     * @param dataStream
     * @return
     */
    public static GraphicalView CreateChart(DataStream dataStream, Context context) {
        String streamId = dataStream.getId();
        if (streamId.equalsIgnoreCase("temp")) {
            TemperatureChart chart = new TemperatureChart(dataStream);
            return chart.execute(context);
        } else if (streamId.equalsIgnoreCase("airpressure")) {
            AirpressureChart chart = new AirpressureChart(dataStream);
            return chart.execute(context);
        } else if (streamId.equalsIgnoreCase("hum")) {
            HumidityChart chart = new HumidityChart(dataStream);
            return chart.execute(context);
        } else if (streamId.equalsIgnoreCase("rainrate")) {
            RainrateChart chart = new RainrateChart(dataStream);
            return chart.execute(context);
        } else if (streamId.equalsIgnoreCase("winddir")) {
            WinddirChart chart = new WinddirChart(dataStream);
            return chart.execute(context);
        } else if (streamId.equalsIgnoreCase("windspeed")) {
            WindspeedChart chart = new WindspeedChart(dataStream);
            return chart.execute(context);
        }

        return null;
    }
}
