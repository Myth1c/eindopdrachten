package nl.myth1cproductions.bredaweerandroid;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import org.achartengine.GraphicalView;

import nl.myth1cproductions.bredaweerandroid.charts.ChartFactory;
import nl.myth1cproductions.bredaweerandroid.data.DataService;
import nl.myth1cproductions.bredaweerandroid.data.model.DataStream;

/**
 * Created by Max on 22-10-13.
 */
public class GraphActivity extends Activity {

    private DataService dataService;
    private GraphicalView currentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        int gradient = getIntent().getExtras().getInt("gradient");
        int actionbar = getIntent().getExtras().getInt("actionbar");
        dataService = (DataService) getIntent().getExtras().getParcelable("dataservice");

        setAppearance(gradient, actionbar);
        loadChartForDatastream(dataService.getTemperatureStream());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_graph_humidity:
                loadChartForDatastream(dataService.getHumidityStream());
                return true;
            case R.id.action_graph_temperature:
                loadChartForDatastream(dataService.getTemperatureStream());
                return true;
            case R.id.action_graph_airpressure:
                loadChartForDatastream(dataService.getAirpressureStream());
                return true;
            case R.id.action_graph_winddirection:
                loadChartForDatastream(dataService.getWinddirectionStream());
                return true;
            case R.id.action_graph_windspeed:
                loadChartForDatastream(dataService.getWindspeedStream());
                return true;
            case R.id.action_graph_rainrate:
                loadChartForDatastream(dataService.getRainrateStream());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.graphs, menu);
        return true;
    }

    private void setAppearance(int gradient, int actionbarColor) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.graph_root);

        Drawable fromGradient = layout.getBackground();
        Drawable toGradient = getResources().getDrawable(gradient);

        layout.setBackgroundResource(gradient);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            ColorDrawable colorDrawable = (ColorDrawable) getResources().getDrawable(actionbarColor);
            ActionBar actionBar;
            actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setBackgroundDrawable(colorDrawable);
                actionBar.setDisplayShowTitleEnabled(false); //Hackaround because the color won't update otherwise when called outside of onCreate.
                actionBar.setDisplayShowTitleEnabled(true);
            }
        }
    }

    private void loadChartForDatastream(DataStream stream)
    {
        if (currentView != null) {
            ((ViewGroup) currentView.getParent()).removeView(currentView);
        }
        GraphicalView view = ChartFactory.CreateChart(stream, this);
        LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
        view.setBackgroundColor(Color.TRANSPARENT);
        view.setId(5);
        view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        layout.addView(view);
        currentView = view;
    }
}
