package nl.myth1cproductions.bredaweerandroid.data;

import java.util.ArrayList;

import nl.myth1cproductions.bredaweerandroid.data.model.DataStream;

/**
 * Created by Max on 4-10-13.
 */
public interface OnDataRequestCompletedListener {
    void onDataRequestCompleted(ArrayList<DataStream> result);
}