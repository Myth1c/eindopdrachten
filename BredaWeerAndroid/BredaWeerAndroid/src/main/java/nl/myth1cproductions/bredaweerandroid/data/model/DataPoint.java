package nl.myth1cproductions.bredaweerandroid.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Max on 4-10-13.
 */
public class DataPoint implements Comparable, Parcelable {
    private double value;
    private Date date;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public DataPoint(JSONObject json) throws JSONException {
        this.value = json.getDouble("value");
        try {
            this.date = dateFormat.parse(json.getString("at"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getDate() {
        return this.date;
    }

    public double getValue() {
        return this.value;
    }

    public SimpleDateFormat getDateFormat() {
        return this.dateFormat;
    }

    @Override
    public int compareTo(Object another) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;
        if (!(another instanceof DataPoint)) {
            throw new IllegalArgumentException("Attempting to compare a non datapoint object to a datapoint object");
        }
        DataPoint anotherPoint = (DataPoint) another;
        return date.compareTo(anotherPoint.getDate()); //Natural sorting is chronological.
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeDouble(value);
        out.writeSerializable(date);
    }

    public static final Parcelable.Creator<DataPoint> CREATOR = new Parcelable.Creator<DataPoint>() {
        public DataPoint createFromParcel(Parcel in) {
            return new DataPoint(in);
        }

        public DataPoint[] newArray(int size) {
            return new DataPoint[size];
        }
    };

    private DataPoint(Parcel in) {
        value = in.readDouble();
        date = (Date) in.readSerializable();
    }
}
