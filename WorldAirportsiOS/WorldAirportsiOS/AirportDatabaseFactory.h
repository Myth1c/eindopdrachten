//
//  AirportDatabaseFactory.h
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/24/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractAirportDatabase.h"

typedef enum{
    REMOTE,
    LOCAL
} AIRPORT_SOURCE_ENUM;

@interface AirportDatabaseFactory : NSObject

+(id<AbstractAirportDatabase>) createAirportDatabaseFromSource:(AIRPORT_SOURCE_ENUM)source;

@end
