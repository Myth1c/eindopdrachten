//
//  Annotation.m
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/25/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AirportAnnotation.h"

@implementation AirportAnnotation

-(id) initWithTitle:(NSString *)name andCoordinate:(CLLocationCoordinate2D)c2d
{
    if( self = [super init] )
    {
        _name = name;
        _coordinate = c2d;
    }
    return self;
}

-(void) dealloc
{
    _name = NULL;
}

@end
