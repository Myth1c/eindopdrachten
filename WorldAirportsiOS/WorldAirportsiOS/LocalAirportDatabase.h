//
//  AirportDatabase.h
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Airport.h"
#import "AbstractAirportDatabase.h"

@interface LocalAirportDatabase : NSObject <AbstractAirportDatabase>

+(LocalAirportDatabase *)getInstance;

-(NSArray*) getAirports;
-(NSMutableArray *) getInfoWithICAO:(NSString *)icao;

@end
