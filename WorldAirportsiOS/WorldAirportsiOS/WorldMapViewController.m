//
//  WorldMapViewController.m
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/24/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "WorldMapViewController.h"
#import "AirportAnnotation.h"

@interface WorldMapViewController ()

@property (nonatomic, strong) id<AbstractAirportDatabase> database;
@property (nonatomic, strong) Airport* selectedFromAirport;
@property (nonatomic, strong) Airport* selectedToAirport;

@end

@implementation WorldMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _worldMap.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self loadAnnotations];
    [self zoomToSelectedAirport];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDatabase:(id<AbstractAirportDatabase>)database
{
    _database = database;
}

- (void)loadAnnotations
{
    AirportAnnotation* annotation = [[AirportAnnotation alloc] initWithTitle:_selectedToAirport.name andCoordinate:_selectedToAirport.location];
    [_worldMap addAnnotation:annotation];
    
    annotation = [[AirportAnnotation alloc] initWithTitle:_selectedFromAirport.name andCoordinate:_selectedFromAirport.location];
    [_worldMap addAnnotation:annotation];
    CLLocationCoordinate2D coordinates[2];
    
    coordinates[0] = _selectedFromAirport.location;
    coordinates[1] = _selectedToAirport.location;
    
    MKGeodesicPolyline * line = [MKGeodesicPolyline polylineWithCoordinates:coordinates count:2];
    [_worldMap addOverlay:line];
}

#pragma mark - iOS7 support
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *polylineView = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor redColor];
    polylineView.lineWidth = 1.0;
    
    return polylineView;
}

#pragma mark - iOS6 support
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor redColor];
    polylineView.lineWidth = 1.0;
    
    return polylineView;
}

- (void)setSelecteFromAirport:(Airport*)fromAirport toAirport:(Airport*)toAirport
{
    _selectedFromAirport = fromAirport;
    _selectedToAirport = toAirport;
}

- (void)zoomToSelectedAirport
{
    //[_worldMap setCenterCoordinate:_selectedAirport.location animated:YES];
    float spanX = 1.0;
    float spanY = 1.0;
    MKCoordinateRegion region;
    region.center.latitude = _selectedToAirport.location.latitude;
    region.center.longitude = _selectedToAirport.location.longitude;
    region.span.latitudeDelta = spanX;
    region.span.longitudeDelta = spanY;
    
    [_worldMap setRegion:region];
}

@end
