//
//  Airport.m
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "Airport.h"

@implementation Airport

@synthesize icoa_code = _icoa_code;
@synthesize name = _name;
@synthesize location = _location;
@synthesize elevation = _elevation;
@synthesize iso_country = _iso_country;
@synthesize municipality = _municipality;

@end
