//
//  Annotation.h
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/25/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AirportAnnotation : NSObject <MKAnnotation>

@property(nonatomic, strong) NSString *name;
@property(nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id) initWithTitle:(NSString *)name andCoordinate:(CLLocationCoordinate2D)c2d;

@end