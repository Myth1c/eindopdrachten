//
//  Airport.h
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Airport : NSObject

@property(nonatomic, copy) NSString *icoa_code;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, assign) CLLocationCoordinate2D location;
@property(nonatomic, assign) double elevation;
@property(nonatomic, copy) NSString *iso_country;
@property(nonatomic, copy) NSString *municipality;


@end
