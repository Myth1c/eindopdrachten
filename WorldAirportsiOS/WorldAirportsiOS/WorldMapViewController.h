//
//  WorldMapViewController.h
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/24/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AbstractAirportDatabase.h"

@interface WorldMapViewController : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *worldMap;

- (void)setDatabase:(id<AbstractAirportDatabase>)database;
- (void)setSelecteFromAirport:(Airport*)fromAirport toAirport:(Airport*)toAirport;

@end
