//
//  AirportTableViewController.m
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/24/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AirportsTableViewController.h"
#import "AbstractAirportDatabase.h"
#import "AirportDatabaseFactory.h"
#import "WorldMapViewController.h"

@interface AirportsTableViewController ()
{
    NSArray *allAirports;
    NSArray *filtered;
    Airport *selectedFrom;
    Airport *selectedTo;
}

@property (nonatomic, strong) id<AbstractAirportDatabase> airportDatabase;

@end

@implementation AirportsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    #warning Hardcoded string.
    [self setTitle:@"Set departure"];
    
    _airportDatabase = [AirportDatabaseFactory createAirportDatabaseFromSource:LOCAL];
    allAirports = [_airportDatabase getAirportNames];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SearchBar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    NSArray *filteredArray = [allAirports filteredArrayUsingPredicate:predicate];
    filtered = filteredArray;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [filtered count];
    }
    return [allAirports count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AirportCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if(tableView == self.searchDisplayController.searchResultsTableView && [filtered count] > 0)
    {
        NSString *airport = [filtered objectAtIndex: indexPath.item];
        
        // Configure the cell...
        [cell.textLabel setText:airport];
        
        if([airport isEqualToString:selectedFrom.name ] || [airport isEqualToString:selectedTo.name ])
        {
            [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
    else
    {
        NSString *airport = [allAirports objectAtIndex: indexPath.row];
    
        // Configure the cell...
        [cell.textLabel setText:airport];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setTitle:@"Set arrival"];
    NSInteger index = indexPath.row;
    
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSString *selected = [filtered objectAtIndex:indexPath.row];
        index = [allAirports indexOfObject:selected];
    }

    Airport* selectedAirport = [_airportDatabase getAirportWithIndex:index];
    if(!selectedFrom && selectedTo != selectedAirport)
    {
        selectedFrom = selectedAirport;
    } else if(!selectedTo && selectedFrom != selectedAirport)
    {
        selectedTo = selectedAirport;
    }
    
    if(selectedFrom && selectedTo)
    {
        [self performSegueWithIdentifier:@"segueToMap" sender:self];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setTitle:@"Set departure"];
    
    NSInteger index = indexPath.row;
    NSString *deselectedObject = [allAirports objectAtIndex:index];
    
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        deselectedObject = [filtered objectAtIndex:index];
        index = [allAirports indexOfObject:deselectedObject];
    }
    
    if([selectedFrom.name isEqualToString:deselectedObject])
    {
        selectedFrom = nil;
    }
    if([selectedTo.name isEqualToString:deselectedObject])
    {
        selectedTo = nil;
    }
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    WorldMapViewController *wmpController = [segue destinationViewController];
    [wmpController setDatabase:_airportDatabase];
    [wmpController setSelecteFromAirport:selectedFrom toAirport:selectedTo];
}

@end
