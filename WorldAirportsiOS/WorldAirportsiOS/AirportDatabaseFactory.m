//
//  AirportDatabaseFactory.m
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/24/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "AirportDatabaseFactory.h"
#import "LocalAirportDatabase.h"

@implementation AirportDatabaseFactory

+(id<AbstractAirportDatabase>) createAirportDatabaseFromSource:(AIRPORT_SOURCE_ENUM)source {
    switch (source)
    {
        case LOCAL:
            return [[LocalAirportDatabase alloc] init];
            break;
            
        default:
            break;
    }
    return nil;
}

@end
