//
//  AbstractAirportDatabase.h
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/24/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Airport.h"

@protocol AbstractAirportDatabase <NSObject>

@required
-(Airport *) getAirportWithIndex:(NSInteger)index;
-(Airport *) getAirportBy:(NSString *)icao;
-(NSInteger) amountOfEntries;
-(NSArray *) getAirports;
-(NSArray *) getAirportNames;

@optional
-(NSArray *) getAirports;

@end
