//
//  AirportDatabase.m
//  WorldAirportsiOS
//
//  Created by atmstudent on 10/11/13.
//  Copyright (c) 2013 atmstudent. All rights reserved.
//

#import "LocalAirportDatabase.h"

@implementation LocalAirportDatabase
{
    sqlite3 *_database;
    NSArray *airportArray;
}

+(LocalAirportDatabase *)getInstance
{
    static LocalAirportDatabase *airportdb =nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        airportdb = [[self alloc]init];
    });
    return airportdb;
}

-(id) init
{
    if( self = [super init] )
    {
        NSString *db = [[NSBundle mainBundle] pathForResource:@"airports" ofType:@"sqlite"];
        if( sqlite3_open([db UTF8String], &_database) != SQLITE_OK )
        {
            NSLog(@"Failed to open database!");
        }
    }
    return self;
}

-(NSArray*) getAirportNames
{
    NSArray *airports = [self getAirports];
    NSMutableArray *airportNames = [[NSMutableArray alloc] init];
    for(Airport* airport in airports)
    {
        [airportNames addObject:airport.name];
    }
    return airportNames;
}

/**
 *Returns all the airports in the database
 *
 */
-(NSArray*) getAirports
{
    if(airportArray)
    {
        return airportArray;
    }
    else
    {
        NSMutableArray *airports = [[NSMutableArray alloc] init];
        NSString *query = [NSString stringWithFormat: @"SELECT * FROM airports ORDER BY name ASC"];
        NSLog(@"Data = %@",query);
    
        sqlite3_stmt *statement;
        int retVal = sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil);
        if( retVal != SQLITE_OK)
        {
            NSLog(@"[SQLITE] Error when preparing query: %d", retVal);
        }
        else
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                Airport *airportInfo = [[Airport alloc] init];
                airportInfo.icoa_code = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 0)];
                airportInfo.name = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 1)];
                CLLocationDegrees longitude = sqlite3_value_double(sqlite3_column_value(statement, 2));
                CLLocationDegrees latitude = sqlite3_value_double(sqlite3_column_value(statement, 3));
                airportInfo.location = CLLocationCoordinate2DMake(latitude, longitude);
                airportInfo.elevation = sqlite3_value_double(sqlite3_column_value(statement, 4));
                airportInfo.iso_country = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 5)];
                airportInfo.municipality = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 6)];
            
                [airports addObject:airportInfo];
            }
            airportArray = airports;
        }
    
        return airports;
    }
}

/**
 *Returns an array with airports that have the given ICAO
 *
 */
-(NSArray *) getInfoWithICAO:(NSString *)icao;
{
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    
    NSString *query = [NSString stringWithFormat: @"SELECT * FROM airports WHERE icao = \"%@\";", [icao uppercaseString]];
    
    sqlite3_stmt *statement;
    if( sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            Airport *airportInfo = [[Airport alloc] init];
            airportInfo.icoa_code = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 0)];
            airportInfo.name = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 1)];
            CLLocationDegrees longitude = sqlite3_value_double(sqlite3_column_value(statement, 2));
            CLLocationDegrees latitude = sqlite3_value_double(sqlite3_column_value(statement, 3));
            airportInfo.location = CLLocationCoordinate2DMake(latitude, longitude);
            airportInfo.elevation = sqlite3_value_double(sqlite3_column_value(statement, 4));
            airportInfo.iso_country = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 5)];
            airportInfo.municipality = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 6)];
            
            [retval addObject:airportInfo];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

-(Airport *) getAirportBy:(NSString *)icao
{
    Airport *retVal = nil;
    
    NSString *query = [NSString stringWithFormat: @"SELECT * FROM airports WHERE icao = \"%@\";", [icao uppercaseString]];
    
    sqlite3_stmt *statement;
    if( sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            Airport *airportInfo = [[Airport alloc] init];
            airportInfo.icoa_code = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 0)];
            airportInfo.name = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 1)];
            CLLocationDegrees longitude = sqlite3_value_double(sqlite3_column_value(statement, 2));
            CLLocationDegrees latitude = sqlite3_value_double(sqlite3_column_value(statement, 3));
            airportInfo.location = CLLocationCoordinate2DMake(latitude, longitude);
            airportInfo.elevation = sqlite3_value_double(sqlite3_column_value(statement, 4));
            airportInfo.iso_country = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 5)];
            airportInfo.municipality = [NSString stringWithUTF8String:(char *) sqlite3_column_text(statement, 6)];
            
            retVal = airportInfo;
            break; //We only want to return 1 value;
        }
        sqlite3_finalize(statement);
    }
    return retVal;
}

-(NSInteger) amountOfEntries {
    if(!airportArray)
    {
        [self getAirports];
    }
    return [airportArray count];
}

-(Airport *)getAirportWithIndex:(NSInteger)index {
    if(!airportArray)
    {
        [self getAirports];
    }
    return [airportArray objectAtIndex:index];    
}


-(void) dealloc {
    sqlite3_close(_database);
}

@end
